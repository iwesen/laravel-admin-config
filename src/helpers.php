<?php

use Encore\Admin\Config\ConfigModel;

if (!function_exists('set_config')) {

    /**
     *
     * @param string $dotKey
     * 
     * @param string $value
     *
     * @return string 'fail'  'success'
     */
    function set_config($dotKey,$value)
    {
        return (new ConfigModel)->setConfig($dotKey,$value);
    }
}

