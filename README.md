A new Config manager for laravel-admin
========================

## 预览

![添加配置项](http://statics.iwesen.com/images/add.jpg)
![配置项列表](http://statics.iwesen.com/images/list.jpg)

## 安装

*版本限制*:由于需要使用laravel-admin的`KeyValue`表单项，所以，laravel-admin需要在1.7.0版本以上；

```
$ composer require iwesen/laravel-admin-config

$ php artisan migrate
```

打开 `app/Providers/AppServiceProvider.php`, 在`boot`方法中调用 `Config::load()` :

```php
<?php

namespace App\Providers;

use Encore\Admin\Config\Config;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    public function boot()
    {
        $table = config('admin.extensions.config.table', 'admin_config');
        if (Schema::hasTable($table)) {
            Config::load();
        }
    }
}
```

引入config扩展: 

```
$ php artisan admin:import config
```

访问  `http://your-host/admin/config`

## 使用方法
1. 添加配置项
当`value`仅有一对键值对，且`key`填写`0`时，默认此配置项的值未字符串；直接调用 `config($key)`即可；
当`value`为多个键值对或只有一对键值对但`key`不是`0`时，默认此配置项的值为数组；调用`config('item.key')`即可。

2. 设置配置值
新增`set_config`方法，可直接修改多维配置项某个键的值；`set_config($dotKey,$value)`即可。


License
------------
Licensed under [The MIT License (MIT)](LICENSE).
